console.log('yo');

var app = angular.module('app', []);
app.controller('appCtrl', ["$scope", "$http", function($scope, $http) {
    $http.get("data.json")
        .then(function(response) {
            var totalChannels = 0;
            var totalDevices = 0;

            for (var d in response.data) {
                totalChannels += response.data[d].Channels;
                totalDevices += response.data[d].Devices;
            }

            $scope.totalChannels = totalChannels;
            $scope.totalDevices = totalDevices;
            $scope.records = response.data;
            $scope.selected = 'All';
        });
}]);
app.filter('selected', function() {
    return function(input, selected) {
        if (!input) return input;
        if (!selected || selected == 'All') return input;

        var expected = ('' + selected).toLowerCase();
        var result = {};
        angular.forEach(input, function(value, key) {
            if (key == selected) {
                result[key] = value;
            }
        });
        return result;
    }
});
