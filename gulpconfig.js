/******* For use with "gulp watch" *******/

// whether or not gulp watch is used in the default "gulp".
var watchInDefault = true;
// the URI to which the browser will open to.
var watchURI = 'mysite.dev';
// which of the below groups of files to watch
var watchStyles = true;
var watchScripts = true;
var watchImages = true;
var watchUploads = false;

/******* For use with "gulp styles" *******/

// whether or not gulp styles is used in the default "gulp".
var stylesInDefault = true;
// the Source Directory which contains the styles files. This is relative to where the gulpfile.js file exists.
var stylesSrcDir = './assets/sass/';
// source style file name. It is not an array to be concatenated because of the @import rule used in Sass/CSS.
var stylesSrcFile = 'main.scss';
// the Distributable Directory that would be created for the concatenated styles file. This is relative to where the gulpfile.js file exists.
var stylesDistDir = './dist/';
// the file name you would like to give to the styles distributable file. The style's gulping will create a concatenated version with this filename.css and it will also create a minified version with the min prefix: filename.min.css
var stylesDistFilename = 'main';

/******* For use with "gulp scripts" *******/

// whether or not gulp scripts is used in the default "gulp".
var scriptsInDefault = true;
// the Source Directory which contains the script files. This is relative to where the gulpfile.js file exists.
var scriptsSrcDir = './assets/js/';
// the order in which these source script files should be concatenated. Remember to include the remaining URI if any files are nested deeper that the source URI defined above.
var scriptsConcatOrder = ['app.js'];
// the scripts plugin glob. This exists so that it may ignore jshint, preventing uneccessary errors from external script plugins. It doesn't matter if none exists.
var scriptsPluginsGlob = './assets/js/vendor/*.js';
// the Distributable Directory that would be created for the concatenated scripts file. This is relative to where the gulpfile.js file exists.
var scriptsDistDir = './dist/';
// the file name you would like to give to the scripts distributable file. The script's gulping will create a concatenated version with this filename.js and it will also create a minified version with the min prefix: filename.min.js
var scriptsDistFilename = 'main';

/*****************************************
 * CONFIGURE END - NO NEED TO EDIT BELOW *
 *****************************************/

function concatWithSrcDir(concat, srcDir) {
  for (var i = 0; i < concat.length; i++) {
    concat[i] = srcDir+concat[i];
  }
  return concat;
}

var defaultsArray = [];
var preWatchArray = [];

if (stylesInDefault) {
  defaultsArray.push('styles');
}

if (scriptsInDefault) {
  defaultsArray.push('scripts');
}

if (watchInDefault) {
  defaultsArray.push('watch');
}

var scriptsConcat = concatWithSrcDir(scriptsConcatOrder, scriptsSrcDir);

var gulpconfig = {
  defaultsArray: defaultsArray,
  scripts: {
    srcDir: scriptsSrcDir,
    concat: scriptsConcat,
    pluginsGlob: scriptsPluginsGlob,
    distDir: scriptsDistDir,
    distFilename: scriptsDistFilename,
  },
  styles: {
    srcDir: stylesSrcDir,
    srcFile: stylesSrcFile,
    distDir: stylesDistDir,
    distFilename: stylesDistFilename,
  },
  watch: {
    proxy: watchURI,
    styles: watchStyles,
    scripts: watchScripts,
  },
};

module.exports = gulpconfig;