// Gulp and it's plugin required
var clean = require('del');
var gulp = require('gulp');
var rename = require('gulp-rename');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var glob = require('gulp-sass-glob');
var minifyCss = require('gulp-clean-css');
var jshint = require('gulp-jshint');
var jsConcat = require('gulp-concat');
var jsMinify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var gulpconfig = require('./gulpconfig');


// Paths for gulp tasks
var distStylesDir = gulpconfig.styles.distDir;
var distScriptsDir = gulpconfig.scripts.distDir;
var distImagesDir = gulpconfig.scripts.distDir;
var srcStylesDir = gulpconfig.styles.srcDir;
var srcStyles = srcStylesDir+gulpconfig.styles.srcFile;
var srcScripts = gulpconfig.scripts.concat;
var srcScriptsDir = gulpconfig.scripts.srcDir;
var distStylesFilename = gulpconfig.styles.distFilename;
var distStylesFile = distStylesDir+distStylesFilename+'.css';
var distScriptsFilename = gulpconfig.scripts.distFilename;
var distScriptsFile = distScriptsDir+distScriptsFilename+'.js';
var scriptsPluginsGlob = gulpconfig.scripts.pluginsGlob;
var watchProxy = gulpconfig.watch.proxy;

// Clean (del) Tasks

gulp.task('clean-styles', function(){
  return clean(distStylesDir);
});

gulp.task('clean-scripts', function(){
  return clean(distScriptsDir);
});

// Styles Tasks

gulp.task('sass', ['clean-styles'], function() {
  return gulp.src(srcStyles)
    .pipe(glob())
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(sass({includePaths: ['./**']}))
    .pipe(sass().on('error', sass.logError))
    .pipe(rename({ basename: distStylesFilename }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(distStylesDir));
});

gulp.task('styles', ['sass'], function(){
  return gulp.src(distStylesFile, {base: './'})
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(plumber())
    .pipe(minifyCss())
    .pipe(rename({ suffix: '.min' }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('.'));
});

// Scripts Tasks

gulp.task('concat-main', [], function(){
  return gulp.src(srcScripts)
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(jsConcat(distScriptsFilename+'.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(distScriptsDir));
});

gulp.task('concat-all', ['concat-main'], function(){
  return gulp.src([scriptsPluginsGlob, distScriptsFile])
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(jsConcat(distScriptsFilename+'.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(distScriptsDir))
});

gulp.task('scripts', ['concat-all'], function(){
  return gulp.src(distScriptsFile, {base: './'})
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(plumber())
    .pipe(jsMinify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('.'));
});

// The Watch tasks
gulp.task('watch', function() {

  if (gulpconfig.watch.styles) {
    gulp.watch(srcStylesDir+'**/*.scss', ['styles']);
  }

  if (gulpconfig.watch.scripts) {
    gulp.watch(srcScriptsDir+'**/*.js', ['scripts']);
  }

  if (gulpconfig.watch.images) {
    gulp.watch(srcImagesDir+'**/*', ['images']);
  }
});

// The Default Task
gulp.task('default', gulpconfig.defaultsArray);